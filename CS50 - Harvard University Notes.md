# CS50 - Harvard University

[CS50 IDE](ide.cs50.io)

## CS50 2020 - Lecture 1 - C

### Hello, world

```c
#include <stdio.h>

int main(void)
{
    printf("Meu código executou!");
}
```

Por convenção os **arquivos em C** devem **terminar** com **`.c`** e os arquivos C++ em `.cpp`.

Computadores usam binário para conversarem, um monte de 1 e 0.

### Compiling

Por mais que eu entenda **o que está escrito** - *por estar em inglês* - **o computador não entende**. Para que ocorra a compreensão por parte da máquina, preciso converter tudo o que foi escrito em vários **0**s e **1**s.

Para tal função, irei necessitar de um compilador.

O comando `make [nome do arquivo]` executa o inicio da compilação.

```c
~/ $ make hello
```

saída:

```c
clang -ggdb3 -O0 -std=c11 -Wall -Werror -Wextra -Wno-sign-compare -Wno-unused-parameter -Wno-unused-variable -Wshadow    hello.c  -lcrypt -lcs50 -lm -o hello
```

após isso :

```c
~/ $ ./hello
```

saída:

```
Meu código executou!~/ $ 
```

### Functions

**Uma função é como um miniprograma**. É uma ação ou um verbo que você pode usar ao escrever seu próprio programa.

O `printf("")` é o mesmo que dizer: imprima formatado.

```c
printf("hello, world");
```

**Funções** são apenas uma **versão programada de um algoritmo**, a implementação de um algoritmo em código, em software. Portanto, uma função pode ser representada como tendo entradas, também conhecidas como **argumentos**.

### Return Values

Conhecido como valores de retorno, em uma função pode realmente apenas devolver um valor. Vai apenas passar para você, programador, um valor que possa ser reutilizado.

### Variables

`String` é o que o cientista de computação chamaria de texto. `get_string()` é uma função que escrevemos que recebe entradas.

Quando você usa `get_string()`, você deseja fazer algo.

Em C, como na maioria das linguagens de programação, é muito mais pedante e se você quer que algo termine em uma variável você tem que fazer isso sozinho.

O sinal de igual (`=`) significa , efetivamente, copiar o que está à direita para o que está a esquerda.

```c
pergunta = get_string("Qual o seu nome? ");
```

No mundo de C, você não pode apenas ter variáveis. Você deve informar ao computador com antecedência que tipo de variável deseja.

### Format Codes

```c
printf("hello, %s", pergunta);
```

O `%s` é o que podemos chamar de código formatado. o `f` em `printf` não imprimi só coisas, ele também pode imprimir códigos de formato. E está é apenas uma sintaxe sofisticada para dizer "imprima um valor real aqui, não imprima literalmente". É um espaço reservado para uma `string`.

O fato da virgula estar fora das aspas é significativo. Ela separa o primeiro argumento do segundo argumento.

```c
#include <stdio.h>

int main(void)
{
    string pergunta = get_string("Qual o seu nome? ");
    printf("Hello, %s", pergunta);
}
```

saída:

```c
~/ $ make hello
clang -ggdb3 -O0 -std=c11 -Wall -Werror -Wextra -Wno-sign-compare -Wno-unused-parameter -Wno-unused-variable -Wshadow    hello.c  -lcrypt -lcs50 -lm -o hello
hello.c:5:5: error: use of undeclared identifier 'string'; did you mean 'stdin'?
    string pergunta = get_string("QUal o seu nome? ");
    ^~~~~~
    stdin
/usr/include/stdio.h:137:14: note: 'stdin' declared here
extern FILE *stdin;             /* Standard input stream.  */
             ^
hello.c:5:11: error: expected ';' after expression
    string pergunta = get_string("QUal o seu nome? ");
          ^
          ;
hello.c:5:5: error: expression result unused [-Werror,-Wunused-value]
    string pergunta = get_string("QUal o seu nome? ");
    ^~~~~~
hello.c:5:12: error: use of undeclared identifier 'pergunta'
    string pergunta = get_string("QUal o seu nome? ");
           ^
hello.c:5:23: error: implicit declaration of function 'get_string' is invalid in C99 [-Werror,-Wimplicit-function-declaration]
    string pergunta = get_string("QUal o seu nome? ");
                      ^
hello.c:6:25: error: use of undeclared identifier 'pergunta'
    printf("Hello, %s", pergunta);
                        ^
6 errors generated.
make: *** [<builtin>: hello] Error 1
```

Tal erro foi apresentado porquê não temos acesso a função `get_string`. Precisamos da biblioteca dele, a `<cs50.h>`.

```c
#include <cs50.h>
#include <stdio.h>

int main(void)
{
    string pergunta = get_string("Qual o seu nome? ");
    printf("Hello, %s \n", pergunta);
}
```

Recompilamos o código:

```c
~/ $ make hello
```

saída: 

```c
clang -ggdb3 -O0 -std=c11 -Wall -Werror -Wextra -Wno-sign-compare -Wno-unused-parameter -Wno-unused-variable -Wshadow    hello.c  -lcrypt -lcs50 -lm -o hello
```

executamos:

```c
~/ $ ./hello
```

saída:

```c
Qual o seu nome? Eduardo
Hello, Eduardo~/ $ 
```

Na linguagem C sempre terá a necessidade de se recompilar o código todas as vezes que for executar. Caso contrário ele terá como saída o a última compilação.

**Conselho:**

- **Usar nomes de variáveis descritivos também é uma boa questão de estilo.**

### `main`

```c
int main(void)
{

}
```

É assim que você começa a escrever um programa.

```c
int main(void)
{
	printf("Hello, world");
}
```



### Header Files

Os arquivos de cabeçalho, como o `#include <stdio.h>`, fornecem acesso a mais funções do que você pode obter da linguagem por padrão.

### `help50`

O `help50` ajuda a encontrar o erro no código.

código que retornará erro:

```
int main(void)
{
	printf("Hello, world");
}
```

chamada do `help50` no console:

```c
~/ $ help50 make hello
```

saída:

```c
clang -ggdb3 -O0 -std=c11 -Wall -Werror -Wextra -Wno-sign-compare -Wno-unused-parameter -Wno-unused-variable -Wshadow    hello.c  -lcrypt -lcs50 -lm -o hello
hello.c:6:2: error: implicitly declaring library function 'printf' with type 'int (const char *, ...)' [-Werror,-Wimplicit-function-declaration]
        printf("Hello, world");
        ^
hello.c:6:2: note: include the header <stdio.h> or explicitly provide a declaration for 'printf'
1 error generated.
make: *** [<builtin>: hello] Error 1

Asking for help...

hello.c:6:2: error: implicitly declaring library function 'printf' with type 'int (const char *, ...)'
[-Werror,-Wimplicit-function-declaration]
        printf("Hello, world");

Did you forget to #include <stdio.h> (in which printf is declared) atop your file?
```

No fim da compilação ele nos sugere inserir o `#include <stdio.h>`e era realmente isso que faltava.

```c
// aqui está o que faltava
#include <stdio.h>

int main(void)
{
	printf("Hello, world");
}
```

### Types

`String` é apenas um deles. Há mais na lista a seguir:

- `bool`
- `char`
- `double`
- `float`
- `int`
- `long`
- `striing`

- entre outros.

Eles permitem que você diga ao computador para não apenas armazenar um valor em uma variável, mas que tipo de valor armazenar em uma varável. Na biblioteca `<cs50.h>` temos várias outras:

- `get_char`
- `get_double`
- `get_float`
- `get_int`
- `get_long`
- `get_string`
- entre outros.

### Format Codes

- `%c` para `char`.
- `%f` será para um valor de `float`.
- `%i` para um valor `int`.
- `%li` para imprimir um inteiro longo, também conhecido como `long`.
- `%s` para imprimir uma `string`.
- entre outros.

O Percentual em C é um espaço usado para a impressão de um único caractere.

### Operators

Em C também é suportado operadores matemáticos.

### Addition

```c
#include <cs50.h>
#include <stdio.h>

int main(void)
{
    int x = get_int("Forneça o valor de x: ");

    int y = get_int("Agora o valor de y: ");

    printf("O resultado da soma é: %i \n", x + y);
}
```

Em `int` apenas é aceito até 32 bits, ou, seja, até 2000000000.

```c
#include <cs50.h>
#include <stdio.h>

int main(void)
{
    long x = get_long("Forneça o valor de x: ");

    long y = get_long("Agora o valor de y: ");

    printf("O resultado da soma é: %li \n", x + y);
}
```

### `trucation`

```c
#include <cs50.h>
#include <stdio.h>

int main(void)
{
    float x = get_float("Forneça o valor de x: ");

    float y = get_float("Agora o valor de y: ");
	
    float z = x / y;
    
    printf("O resultado da divisão é: %f \n", z);
}
```

```c
#include <cs50.h>
#include <stdio.h>

int main(void)
{
    int x = get_int("Forneça o valor de x: ");

    int y = get_int("Agora o valor de y: ");
	
    float z = (float) x / (float) y;
    
    printf("O resultado da divisão é: %i \n", z);
}
```

Ao invés de colocarmos os receptores de informações como `float`, podemos apenas converte-los na última `string`.

### Agree

```c
#include <cs50.h>
#include <stdio.h>

int main(void)
{
    char proposta = get_char("Você aceita os termos propostos? ");
    if (proposta == 's' || proposta == 'S')
    {
        printf("Proposta aceita. \n");
    }
    else if (proposta == 'n' || proposta == 'N')
    {
        printf("Proposta recusada! \n");
    }
    else
    {
        printf("Você digitou %c. Deveria ter digitado sim ou não! \n", proposta);
    }
}
    
```

Dessa vez utilizamos aspas simples porquê em C quando comparamos caracteres individuais. `Char` são caracteres individuais.

### Loops

Se você deseja fazer com que algo aconteça para sempre, esta é a maneira mais canônica de expressar algo para sempre. Podemos fazer isso em C usando o que será chamado de `loop for` ou `loop while`. 

```c
#include <stdio.h>
#include <cs50.h>

int main(void)
{
    int printer = 1;
	while (printer <= 50)
    {
        printf("Print número %i \n", printer);
        printer++;
    }
}
```

```c
#include <stdio.h>
#include <cs50.h>

int main(void)
{
    for (int printer = 1; printer <= 50; printer = printer++)
    {
        printf("Print de número: %i \n", printer);
    }
}
```

