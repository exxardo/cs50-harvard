#include <cs50.h>
#include <stdio.h>

int main(void)
{
    int x = get_int("Forneça o valor de x: ");

    int y = get_int("Agora o valor de y: ");
	
    float z = (float) x / (float) y;
    
    printf("O resultado da divisão é: %f \n", z);
}