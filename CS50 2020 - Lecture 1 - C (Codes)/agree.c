#include <cs50.h>
#include <stdio.h>

int main(void)
{
    char proposta = get_char("Você aceita os termos propostos? ");
    if (proposta == 's' || proposta == 'S')
    {
        printf("Proposta aceita. \n");
    }
    else if (proposta == 'n' || proposta == 'N')
    {
        printf("Proposta recusada! \n");
    }
    else
    {
        printf("Você digitou %c. Deveria ter digitado sim ou não! \n", proposta);
    }
}