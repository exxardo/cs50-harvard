#include <stdio.h>
#include <cs50.h>

int main(void)
{
    int altura, degrau, coluna, recuo;

    do
    {
        // Recebe a informação do usuário
        altura = get_int("Defina a altura: ");
    }

    // Loop para gerar degraus
    while (altura < 1 || altura > 8);

    for (degrau = 0; degrau < altura; degrau++)
    {

        // Gerar recuos
        for (recuo = 0; recuo < altura - degrau - 1; recuo++)
        {
            printf(" ");
        }

        for (coluna = 0; coluna <= degrau; coluna++)
        {
            printf("#");
        }

        printf("\n");

    }
}